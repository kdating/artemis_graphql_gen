import 'dart:async';

import 'package:artemis/artemis.dart';
import 'graphql/simple_query.dart';

Future<void> main() async {
  final client = ArtemisClient(
    'https://christian-blind-date-dev.appspot.com/graphql',
  );

  final simpleQuery = SimpleQueryQuery();

  final simpleQueryResponse = await client.execute(simpleQuery);

  print('Simple query response: ${simpleQueryResponse.data.pokemon.number}');

}
